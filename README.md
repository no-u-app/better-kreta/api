# Better Kréta API

DISCLAIMER: Everything until git commit `edef0368418d314e6f0eace23998e3e8bdf081c9` is [daaniiieel](https://github.com/daaniiieel)'s property.

> A wrapper for KRÉTA's v3 REST API, used by Better Kréta

## Discontinued

The project has been discontinued, or better yet never started. I would say 90% of the credit should go to the original author, [daaniiieel](https://github.com/daaniiieel).

## Install

```sh
npm install kreta-api
```

## Example

The best way to try this out is on [RunKit](https://npm.runkit.com/). You can see your code and the documentation side by side!

_Caution: All documents on runKit are **PUBLIC**. Don't use it with personal details! (only use it to get a hang of the module)_

```js
const { KretaApi } = require("kreta-api");

const api = new KretaApi("nice user agent");

api.getSchoolList().then(data => console.log(data));

api.authenticate("username", "pass", "schoolid").then(data => {
    const accessToken = data.access_token;
    const refreshToken = data.refresh_token;

    api.getTimeTable("2020-06-01", "2020-06-02").then(data =>
        console.log(data)
    );

    api.refreshToken().then(data => console.log(data));
});
```

## Usage

The module is intended to be used to communicate with the API in alternative clients. Most of the known API methods are covered.

Every method of the module returns a native JavaScript `Promise`, which is after the API responds, fulfilled with an `Object` containing the server response or a `String` containing an error.

Every function's first parameter is the `ua`, the `Use-Agent` which the wrapper should send to the server.

-   `getSchoolList()` gets the school list.

-   `authenticate(username, password, instituteCode)` authenticates the user with the given details.

-   `refreshToken()` requests a new access_token from the server, but instead of using traditional login details, it uses the refresh_token you get with the `authenticate()` method.

-   `getTimeTable(fromDate, toDate)` requests the user's time table from the server. Because of the effort to keep the package as minimal as possible, it only accepts dates in the `yyyy-mm-dd` format (for example `2020-06-01`), which is also what the API wants.

-   `getGrades()` requests the user's grades. Nothing too special here.

-   `getAverages(oktatasiNevelesiFeladatUid)` requests the user's averages. **NEW**: The request URL had a typo in it below version 1.1.2. Please update!

-   `getClassAverages(oktatasiNevelesiFeladatUid)` requests the class averages of the user's class. **NEW**: The request URL had a typo in it below version 1.1.2. Please update!

-   `getNotes()` requests the user's notes (feljegyzések in Hungarian, so you're not confused).

-   `getNoticeBoard()` requests the user's noticeboard (faliújság in Hungarian, so you're not confused).

-   `getHomework(fromDate)` requests the user's homeworks. The date format is the same as above: `yyyy-mm-dd`. It's weird that it has no endDate, you'll need to handle that yourself.

-   `getAbsences()` requests the user's absences (mulasztások in Hungarian).

-   `getHomeworkComments(homeworkId)` requests the comments on a homework. You can get the homework id from `getHomework`.

-   `commentOnHomework(homeworkId, commentText)` writes a comment to a homework.

-   `getTests()` requests the tests that the user has been "assigned".

-   `getGroups()` requests the groups within the user's class. There is a chance that you'll get nothing, if the group only consists of one group.

-   `getUserData()` requests user info, such as birth date, mothers name, address, etc.

-   `getUserData()` requests user info, such as birth date, mothers name, address, etc.

-   `getHeadTeacherData()` requests info about the user's head teacher (olsztályfőnök). I don't see any practical use to it, but hey, they have an endpoint.

-   `getYearOrder()` requests the year order (tanév rendje) of the school. For me it 404's, but for others, it might work.

-   `getMessages()` gets the user's E - Ügyintézés messages (sent, received, deleted) from the server.

-   `getReceivedMessages()` gets the user's received E - Ügyintézés messages from the server.

-   `getSentMessages()` gets the user's sent E - Ügyintézés messages from the server.

-   `getDeletedMessages(ua, access_token)` gets the user's deleted E - Ügyintézés messages from the server.

-   `getUnreadMessages(ua, access_token)` gets the count of the user's unread E - Ügyintézés messages.

-   `getReceivedMessages(ua, access_token)` gets the user's received E - Ügyintézés messages from the server.

-   `getMessageContent(ua, access_token, messageId)` gets the content of a single message. You can get `messageId` from any of the `get*Messages` methods.

-   `getAdresseeTypes(ua, access_token)` gets the adressee types (címezhető típusok) from E - Ügyintézés. For me it's always an empty string.

-   `getTeacherData(ua, access_token)` gets the data of teachers from E - Ügyintézés.

-   `getHeadTeacherMessagingData(ua, acessToken)` gets the data of the user's head teacher (osztályfőnök) from E - Ügyintézés. For me, it's always an empty string.

-   `getDirectorateData(ua, acessToken)` gets the data of the directoriate from E - Ügyintézés.

-   `getAdministratorData(ua, acessToken)` gets the data of the admins from E - Ügyintézés.

-   `getMessagingGroupsData(ua, acessToken)` gets the data of the adressee groups (címezhető csoportok) from E-Ügyintézés. For me it 404s, for others it might work.

-   `getMessagingClassesData(ua, acessToken)` gets the data of the adressee classes (címezhető osztályok) from E-Ügyintézés. For me it 404s, for others it might work.

-   `getSzmkRepresentativeData(ua, acessToken)` gets the data of the SZMK representatives (SZMK képviselők) from E-Ügyintézés.

-   `downloadFile(ua, acessToken, fileID)` Downloads a file (attachment of a message) from E-Ügyintézés.

## 🚫 Not implemented yet

-   E-Ügyintézés: sending a message, uploading files.
-   Szülői E-Ügyintézés: everything ()
-   Házi feladatok: Házi feladat Törlése, Késznek Jelölése

## ❗ Errors

After release **1.1**, those errors will no longer occur, since it only returns an `Object`, if the JSON is valid. In the future, the plan is to reject the promise with the error.

In versions before **1.1**:

> If you get an error about `unexpected i at position 0 in JSON` it's most likely a string answer, `invalid_grant`. Your access_token is incorrect. If the error is `unexpected < at position 0 in JSON`, it's JSON.parse not being able to parse HTML.

## Author

Original author:

👤 **Daniel Vanko**

-   Github: [@daaniiieel](https://github.com/daaniiieel)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/no-u-app/better-kreta/kreta-api/-/issues). You can also submit a change at the [Merge Requests tab](https://gitlab.com/no-u-app/better-kreta/kreta-api/-/merge_requests)

## Show your support

Give a ⭐️ if this project helped you!

