const axios = require("axios").default;
var qs = require("querystring");

function parseResponse(string) {
    // parses the response, outputs a JSON object if the response is an
    //object (no errors), outputs a string if it's not
    try {
        var o = typeof string == "string" ? JSON.parse(string) : string;

        // another layer of parsing
        if (o && typeof o === "object") {
            return o;
        }
    } catch (err) {
        console.log(err);
    }

    return string;
}

function KretaApi(ua) {
    this.getSchoolList = function () {
        return new Promise(async function (resolve, reject) {
            const apiUrl = (
                await axios({
                    url:
                        "https://kretamobile.blob.core.windows.net/configuration/ConfigurationDescriptor.json"
                })
            ).data.GlobalMobileApiUrlPROD;

            axios({
                url: apiUrl + "/api/v3/Institute",
                headers: {
                    "User-Agent": ua,
                    apiKey: "7856d350-1fda-45f5-822d-e1a2f3f1acf0"
                }
            })
                .then(res => {
                    resolve(parseResponse(res.data));
                })
                .catch(err => reject(err));
        });
    };

    this.authenticate = function (username, password, instituteCode) {
        return new Promise(function (resolve, reject) {
            this.schoolId = instituteCode;

            // POST JSON body
            var postData = qs.stringify({
                userName: username,
                password: password,
                institute_code: this.schoolId,
                grant_type: "password",
                client_id: "kreta-ellenorzo-mobile"
            });
            const postJsonOptions = {
                url: "https://idp.e-kreta.hu/connect/token",
                headers: {
                    "User-Agent": ua,
                    "Content-Type": "application/x-www-form-urlencoded"
                },

                method: "POST",
                data: postData
            };
            axios(postJsonOptions)
                .then(res => {
                    const parsed = parseResponse(res.data);

                    this.accessToken = parsed.access_token;
                    this.refreshToken = parsed.refresh_token;
                    resolve(parsed);
                })
                .catch(err => reject(err));
        });
    };

    this.refreshToken = function () {
        return new Promise(function (resolve, reject) {
            // POST JSON body
            var postData = qs.stringify({
                refresh_token: this.refreshToken,
                institute_code: this.schoolId,
                grant_type: "refresh_token",
                client_id: "kreta-ellenorzo-mobile"
            });
            const postJsonOptions = {
                url: "https://idp.e-kreta.hu/connect/token",
                headers: {
                    "User-Agent": ua,
                    "Content-Type": "application/x-www-form-urlencoded"
                },

                method: "POST",
                data: postData
            };
            axios(postJsonOptions)
                .then(res => {
                    const parsed = parseResponse(res.data);

                    this.accessToken = parsed.access_token;
                    resolve(parsed);
                })
                .catch(err => reject(err));
        });
    };

    this.getTimeTable = function (fromDate, toDate) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/OrarendElemek?datumTol=" +
                    fromDate +
                    "&datumIg=" +
                    toDate,
                headers: {
                    Authorization: "Bearer " + this.accessToken,
                    "User-Agent": ua
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getGrades = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/Ertekelesek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getAverages = function (oktatasiNevelesiFeladatUid) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/Ertekelesek/Atlagok/TantargyiAtlagok?oktatasiNevelesiFeladatUid=" +
                    oktatasiNevelesiFeladatUid,
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getClassAverages = function (oktatasiNevelesiFeladatUid) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/Ertekelesek/Atlagok/OsztalyAtlagok?oktatasiNevelesiFeladatUid=" +
                    oktatasiNevelesiFeladatUid,
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getNotes = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/Feljegyzesek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getNoticeBoard = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/FaliujsagElemek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getHomework = function (fromDate) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/HaziFeladatok?datumTol=" +
                    fromDate,
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getAbsences = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/Mulasztasok",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getHomeworkComments = function (homeworkId) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/HaziFeladatok/" +
                    homeworkId +
                    "/Kommentek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.commentOnHomework = function () {
        return new Promise(function (resolve, reject) {
            // POST JSON body

            const postJsonOptions = {
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/Orak/TanitasiOrak/HaziFeladatok/Kommentek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                },

                method: "POST",
                bodyJson: {
                    HaziFeladatUid: homeworkId,
                    FeladatSzovege: commentText
                }
            };
            axios(postJsonOptions)
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getTests = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/BejelentettSzamonkeresek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getGroups = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/OsztalyCsoportok",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getUserData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/TanuloAdatlap",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getHeadTeacherData = function (teacherId) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Felhasznalok/Alkalmazottak/Tanarok/Osztalyfonokok?Uids=" +
                    teacherId,
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getYearOrder = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://" +
                    this.schoolId +
                    ".e-kreta.hu/ellenorzo/V3/Sajat/TanevRendjeElemek",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getMessages = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/postaladaelemek/sajat",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getReceivedMessages = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/postaladaelemek/beerkezett",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getSentMessages = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/postaladaelemek/elkuldott",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getDeletedMessages = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/postaladaelemek/torolt",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getUnreadCount = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/postaladaelemek/olvasatlanokszama",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getMessageContent = function (messageId) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/postaladaelemek/" +
                    messageId,
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getAdresseeTypes = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/adatszotarak/cimeztttipusok",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getTeacherData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kreta/alkalmazottak/tanar",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getHeadTeacherMessagingData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kreta/alkalmazottak/osztalyfonokok",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getDirectorateData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kreta/alkalmazottak/igazgatosag",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getAdministratorData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kreta/alkalmazottak/adminisztrator",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getMessagingGroupsData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/tanoraicsoportok/cimezheto",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getMessagingClassesData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/tanoraicsoportok/cimezheto",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.getSzmkRepresentativeData = function () {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/kommunikacio/szmkkepviselok/cimezheto",
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(parseResponse(res.data)))
                .catch(err => reject(err));
        });
    };

    this.downloadFile = function (fileID) {
        return new Promise(function (resolve, reject) {
            axios({
                url:
                    "https://eugyintezes.e-kreta.hu/api/v1/dokumentumok/uzenetek/" +
                    fileID,
                headers: {
                    "User-Agent": ua,
                    Authorization: "Bearer " + this.accessToken,
                    "X-Uzenet-Lokalizacio": "hu-HU"
                }
            })
                .then(res => resolve(res.data))
                .catch(err => reject(err));
        });
    };
}

module.exports = { KretaApi };
